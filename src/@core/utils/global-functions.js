/* eslint-disable func-names */
import Vue from 'vue'
import axiosIns from '@axios'
// import ToastificationContent from '@/components/toastification/ToastificationContent.vue'

Vue.prototype.showModal = function (val) {
  this.$modal.show(val)
}
Vue.prototype.hideModal = function (val) {
  this.$modal.hide(val)
}

Vue.prototype.getCep = function (cep) {
  axiosIns.get(`https://viacep.com.br/ws/${cep}/json/`).then(res => res)
}

Vue.prototype.maskPhone = function (data) {
  const el = data.toString().split('')
  if (data.length === 11) {
    el.splice(8, 0, '-')
    el.splice(0, 0, '(')
    el.splice(3, 0, ')')
  } else {
    el.splice(7, 0, '-')
    el.splice(0, 0, '(')
    el.splice(3, 0, ')')
  }
  return el.join().replaceAll(',', '')
}

Vue.prototype.getDatasInDate = (typed = 1, date) => {
  const theDate = new Date(date)
  switch (typed) {
    case 1:
      console.log(theDate)
      break

    default:
      break
  }
}

// Vue.prototype.toast = function (config) {
//   this.$toast({
//     component: ToastificationContent,
//     props: {
//       title: config.title,
//       icon: config.icon,
//       text: config.text,
//       variant: config.variant ? config.variant : null,
//       colorIcon: config.colorIcon,
//     },
//   })
// }
